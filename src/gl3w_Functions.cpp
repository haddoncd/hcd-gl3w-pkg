/*

  This file was generated with gl3w_gen.py, part of gl3w
  (hosted at https://github.com/haddoncd/gl3w)

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

*/

#ifdef gl3w_Functions_cpp
#error Multiple inclusion
#endif
#define gl3w_Functions_cpp

#ifndef gl3w_Functions_hpp
#include "gl3w_Functions.hpp"
#endif

#ifndef print_hpp
#error "Please include print.hpp before this file"
#endif

namespace gl3w
{
  namespace error
  {
    Str name[ENUM_COUNT]
    {
      "Invalid Enum"_s,
      "Invalid Value"_s,
      "Invalid Operation"_s,
      "Invalid Framebuffer Operation"_s,
      "Out of Memory"_s,
      "Stack Underflow"_s,
      "Stack Overflow"_s,
      "Unknown"_s
    };

    Enum fromGLenum(GLenum gl_enum)
    {
      switch (gl_enum)
      {
        case GL_INVALID_ENUM:                  return INVALID_ENUM;
        case GL_INVALID_VALUE:                 return INVALID_VALUE;
        case GL_INVALID_OPERATION:             return INVALID_OPERATION;
        case GL_INVALID_FRAMEBUFFER_OPERATION: return INVALID_FRAMEBUFFER_OPERATION;
        case GL_OUT_OF_MEMORY:                 return OUT_OF_MEMORY;
        case GL_STACK_UNDERFLOW:               return STACK_UNDERFLOW;
        case GL_STACK_OVERFLOW:                return STACK_OVERFLOW;
        default:                               return UNKNOWN;
      }
    }
  }

  void Functions::checkExtensions(Str *required, bool *required_found, u32 required_count)
  {
    Functions *gl = this;

    for (u32 i = 0; i < required_count; ++i)
    {
      required_found[i] = false;
    }

    GLint extensions_count;
    gl->GetIntegerv(GL_NUM_EXTENSIONS, &extensions_count);

    for (i32 i = 0; i < extensions_count; ++i)
    {
      Str extension = zStr((const char *)gl->GetStringi(GL_EXTENSIONS, i));

      for (u32 j = 0; j < required_count; ++j)
      {
        if ((!required_found[j]) &&
            (extension == required[j]))
        {
          required_found[j] = true;
        }
      }
    }
  }

  void Functions::checkError()
  {
    Functions *gl = this;

    GLenum gl_error = gl->GetError();

    if (gl_error != GL_NO_ERROR)
    {
      error::Enum error = error::fromGLenum(gl_error);
      println(stderr, "OpenGL Error: "_s, error::name[error]);
      fflush(0);
      assert(false);
    }
  }
}