/*

  This file was generated with gl3w_gen.py, part of gl3w
  (hosted at https://github.com/haddoncd/gl3w)

  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

*/

#ifdef gl3w_Library_hpp
#error Multiple inclusion
#endif
#define gl3w_Library_hpp

#ifndef gl3w_Functions_hpp
#error "Please include gl3w_Functions.hpp before this file"
#endif

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifdef _WIN32
  typedef HINSTANCE__ *HMODULE;
#else
  #error "TODO: Support non-windows platforms"
#endif

namespace gl3w
{
  struct Library
  {
    HMODULE libgl;

    void init();

    void shutdown();

    void *getProc(const char *proc);

    bool getFunctions(Functions *functions);
  };
}
